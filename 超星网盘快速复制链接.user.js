// ==UserScript==
// @name         超星网盘快速复制链接
// @description  一键获取超星网盘的文件外链和直链并复制到剪切板（支持选择多个文件）
// @version      0.3
// @author       yukaidi
// @icon         https://pan-yz.chaoxing.com/favicon.ico
// @match        http://pan-yz.chaoxing.com/
// @require      https://cdn.bootcss.com/clipboard.js/1.5.16/clipboard.min.js
// @namespace http://tampermonkey.net/
// ==/UserScript==

(function() {
    'use strict';
    var div = document.getElementsByClassName('ypActionBar')[0];
    if(div){
        var download_btn = document.createElement("a");
        download_btn.innerText = '复制链接';
        download_btn.setAttribute('href', 'javascript:void(0);');
        download_btn.setAttribute('class', 'fl download opt_btn copyBtn');
        download_btn.setAttribute('id', 'copy_direct_url_btn');
        download_btn.setAttribute('onclick', 'res.copy_direct_url();');
        download_btn.setAttribute('data-clipboard-text', '');
        download_btn.setAttribute('data-clipboard-action', 'copy');
        div.append(download_btn);
        document.getElementById('container').setAttribute('onclick', 'document.getElementById(\'copy_direct_url_btn\').setAttribute(\'style\',\'\');');
        res.copy_direct_url = function(){
            function copyToClipboard(str) {
                document.getElementById('copy_direct_url_btn').setAttribute('data-clipboard-text', str);
                var clipboard = new Clipboard(".copyBtn");
                clipboard.on('success',function(e){
                    console.log('复制成功！');
                });

                clipboard.on('error',function(e){
                    console.log('复制失败！');
                });
            }
            if(res.choosedlen>0){
                var failedFilenames = new Array();
                var failedUrlsAmount = 0;
                var succeededUrls = new Array();
                var succeededFilenames = new Array();
                var succeededUrlsAmount = 0;
                for(var fleid in res.choosed){
                    if(res.choosed[fleid]['type']==1){
                        //var download_url = "http://pan-yz.chaoxing.com/download/downloadfile?fleid="+fleid+"&puid="+res.choosed[fleid]['puid'].toString();
                        var filename = res.choosed[fleid]['name'];
                        var download_url = "http://pan-yz.chaoxing.com/download/downloadfile?fleid="+ fleid +"&puid=1";
                        succeededUrls[succeededUrlsAmount] = download_url;
                        succeededFilenames[succeededUrlsAmount] = filename;
                        succeededUrlsAmount = succeededUrlsAmount+1;
                    }
                    else{
                        failedFilenames[failedUrlsAmount] = res.choosed[fleid]['name'];
                        failedUrlsAmount = failedUrlsAmount+1;
                    }
                }
                if(succeededUrlsAmount>0){
                    var str=succeededUrls[0];
                    for(var i=1;i<succeededUrlsAmount;i=i+1){
                        str=str+'\n'+succeededUrls[i];
                    }
                    copyToClipboard(str);
                    alert(succeededFilenames.toString()+' 等文件的链接已复制到剪切板！');
                }
                if(failedUrlsAmount>0){
                    alert(failedFilenames.toString()+' 等文件请求外链失败！（不支持文件夹）')
                }
            }
            else{
                alert('未选中任何文件！');
            }
        }
    }
})();
